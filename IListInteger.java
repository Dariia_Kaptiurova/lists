package com.company;

import java.util.List;

public interface IListInteger {
    void clear();
    int size();
    int get(int index);
    boolean add(int number);
    boolean add(int index, int number);
    int remove(int number);
    int removeByIndex(int index);
    boolean contains(int number);
    String print();
    int[] toArray();
    List<Integer> subList(int fromIndex, int toIndex);
    boolean removeAll(int[] arr);
    boolean retainAll(int[] arr);
}
