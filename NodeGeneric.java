package com.company;

class NodeGeneric <E> {
    E val;
    NodeGeneric <E> next;
    NodeGeneric <E> prev;

    public NodeGeneric(E val) {
        this.val = val;
        next = null;
        prev = null;
    }

}

