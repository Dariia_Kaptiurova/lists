package com.company;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AListGenericsTest {

    @Test
    void clear_ShouldDeleteAllElements() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);

        //when
        aListGenerics.clear();

        //then
        assertEquals(0, aListGenerics.size());
        assertNotEquals(10, aListGenerics.size());
    }

    @Test
    void size_ShouldReturnSize() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);


        //when
        int expected = 3;

        //then
        assertEquals(expected, aListGenerics.size());
        assertNotEquals(0, aListGenerics.size());
    }

    @Test
    void get_ShouldReturnElementByIndex() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int index = 2;

        //when
        int expected = 3;

        //then
        assertEquals(expected, aListGenerics.get(index));
        assertNotEquals(2, aListGenerics.get(index));
    }

    @Test
    void add_ShouldAddElement() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aListGenerics.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void add_ShouldAddElementByIndex() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int number = 2;
        int index = 1;

        //when
        boolean expected = true;
        boolean actual = aListGenerics.add(index, number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void remove_ShouldRemoveElement() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int number = 2;

        //when
        int actual = aListGenerics.remove(number);

        //then
        assertEquals(number, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void removeByIndex_ShouldRemoveElementByIndex() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int index = 1;

        //when
        int actual = aListGenerics.removeByIndex(index);

        //then
        assertEquals(index, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void contains_ShouldCheckElement() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aListGenerics.contains(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void print_ShouldPrintList() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);

        //when
        String expected = "[1,2,3]";
        String actual = aListGenerics.print();

        //then
        assertEquals(expected, actual);
        assertNotEquals("[]", actual);
    }

    @Test
    void toArray_ShouldGetListAsAnArray() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);

        //when
        Integer[] expected = {1, 2, 3};
        Integer[] actual = aListGenerics.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, Arrays.toString(actual));
    }

    @Test
    void subList_ShouldRemoveThePartOfTheList() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        int fromIndex = 1;
        int toIndex = 2;

        //when
        Object[] expected = {2, 3};
        List<Integer> newList = aListGenerics.subList(fromIndex, toIndex);
        Object[] actual = newList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, actual);
    }

    @Test
    void removeAll_ShouldGetAllElementsWhichAreInBothLists() {

        //given
        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        Integer[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aListGenerics.removeAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void retainAll_ShouldGetAllElementsWhichDoNotRepeatedInBothLists() {

        Integer[] list = {1, 2, 3};
        AListGenerics<Integer> aListGenerics = new AListGenerics<Integer>(list);
        Integer[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aListGenerics.retainAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }
}