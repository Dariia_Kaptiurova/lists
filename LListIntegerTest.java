package com.company;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LListIntegerTest {
    @Test
    void clear_ShouldDeleteAllElements() {
    }

    @Test
    void size_ShouldReturnSize() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);

        //when
        int expected = 3;

        //then
        assertEquals(expected, lListInteger.size());
        assertNotEquals(0, lListInteger.size());
    }

    @Test
    void get_ShouldReturnElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int index = 2;

        //when
        int expected = 3;

        //then
        assertEquals(expected, lListInteger.get(index));
        assertNotEquals(2, lListInteger.get(index));
    }

    @Test
    void add_ShouldAddElement() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lListInteger.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void add_ShouldAddElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;
        int index = 1;

        //when
        boolean expected = true;
        boolean actual = lListInteger.add(index, number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void remove_ShouldRemoveElement() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;

        //when
        int actual = lListInteger.remove(number);

        //then
        assertEquals(number, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void removeByIndex_ShouldRemoveElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int index = 1;

        //when
        int actual = lListInteger.removeByIndex(index);

        //then
        assertEquals(index, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void contains_ShouldCheckElement() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lListInteger.contains(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void print_ShouldPrintList() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);

        //when
        String expected = "[1,2,3]";
        String actual = lListInteger.print();

        //then
        assertEquals(expected, actual);
        assertNotEquals("[]", actual);
    }

    @Test
    void toArray_ShouldGetListAsAnArray() {

        //given
        int[] list = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);

        //when
        int[] expected = {1, 2, 3};
        int[] actual = lListInteger.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, Arrays.toString(actual));
    }

    @Test
    void subList_ShouldRemoveThePartOfTheList() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        LListInteger lListInteger = new LListInteger(list);
        int fromIndex = 2;
        int toIndex = 3;

        //when
        int[] expected = {3, 4};
        List<Integer> newList = lListInteger.subList(fromIndex, toIndex);
        Object[] actual = newList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, actual);
    }

    @Test
    void removeAll_ShouldGetAllElementsWhichAreInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        LListInteger lListInteger = new LListInteger(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = lListInteger.removeAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void retainAll_ShouldGetAllElementsWhichDoNotRepeatedInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        LListInteger lListInteger = new LListInteger(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = lListInteger.retainAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void addStart() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lListInteger.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void addEnd() {

        //given
        int list[] = {1, 2, 3};
        LListInteger lListInteger = new LListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = lListInteger.addEnd(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }
}