package com.company;

import java.util.Arrays;
import java.util.List;

public class AListInteger implements IListInteger {

    private static final int CAPACITY = 10;
    static Integer list[];

    public AListInteger(Integer[] list) {
        AListInteger.list = new Integer[CAPACITY];
    }

    public AListInteger(int[] array) {

        if (array.length == 0) {
            list = new Integer[array.length];
        } else {
            list = new Integer[array.length + (array.length / 100 * 20)];
        }
        for (int i = 0; i < array.length; i++) {
            list[i] = array[i];
        }
    }


    @Override
    public void clear() {
        list = null;
    }

    @Override
    public int size() {
        try {
            return list.length;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public int get(int index) {
        return list[index];
    }

    @Override
    public boolean add(int number) {
        int[] newArray = new int[list.length + 1 + (list.length / 100 * 20)];
        int i;
        for (i = 0; i < list.length; i++) {
            newArray[i] = list[i];
        }
        if (i == list.length) {
            newArray[i] = number;
        }

        String arrayToString = Arrays.toString(newArray);
        char expected = arrayToString.charAt(newArray.length); //кол.эл*3 +1 - поиск индекса чара, поиск вставленного числа в новом массиве
        char numberToChar = (char) (number + '0');
        if (numberToChar == expected) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean add(int index, int number) {
        int[] newArray = new int[list.length + 1 + (list.length / 100 * 20)];
        int i;
        for (i = 0; i < index; i++) {
            newArray[i] = list[i];
        }
        if (i == index) {
            newArray[i] = number;
            i++;
        }
        for (int j = i; j > index && j < newArray.length; j++) {
            newArray[j] = list[j - 1];
        }

        String arrayToString = Arrays.toString(newArray);
        char expected = arrayToString.charAt(index * 3 + 1); //кол.эл*3 +1 - поиск ндекса чара, поиск вставленного числа в новом массиве
        char numberToChar = (char) (number + '0');
        if (numberToChar == expected) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int remove(int number) {
        int[] newArray = new int[list.length - 1];
        int i;
        int index = 0;
        for (int k = 0; k < list.length; k++) {
            if (list[k] == number) {
                index = list[k];
            }
        }
        for (i = 0; i < index - 1; i++) {
            newArray[i] = list[i];
        }
        for (int j = i + 1; j > index && j < newArray.length; j++) {
            newArray[j] = list[j];
        }

        String arrayToString = Arrays.toString(list);
        char expected = arrayToString.charAt(list[i - 1]);
        String arrToString = Arrays.toString(newArray);
        char actual = arrToString.charAt(newArray[i - 1]);
        if (expected == actual) {
            return number;
        } else {
            return 0;
        }
    }

    @Override
    public int removeByIndex(int index) {
        int[] newArray = new int[list.length - 1];
        int i;
        for (i = 0; i < index; i++) {
            newArray[i] = list[i];
        }
        for (int j = i + 1; j > index && j < newArray.length; j++) {
            newArray[j] = list[j];
        }

        String arrayToString = Arrays.toString(list);
        char expected = arrayToString.charAt(list[i-1]);
        String arrToString = Arrays.toString(newArray);
        char actual = arrToString.charAt(newArray[i-1]);
        if (expected == actual) {
            return index;
        } else {
            return 0;
        }
    }

    @Override
    public boolean contains(int number) {
        boolean result = false;
        for (int i = 0; i < list.length; i++) {
            if (number == list[i]) {
                result = true;
                break;
            } else result = false;
        }
        System.out.println(result);
        return result;
    }

    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        if (list == null) {
            sb.append(" ");
        } else {

            for (int i = 0; i < list.length; i++) {

                if (i == list.length) {
                    if (list[i] != null)
                        sb.append(list[i]);
                } else {
                    if (list[i] != null)
                        sb.append(list[i]).append(",");
                }
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        System.out.println(sb.toString());
        return sb.toString();
    }

    @Override
    public int[] toArray() {
        int[] array = new int[list.length];
        for (int i = 0; i < list.length; i++) {
            array[i] = list[i];
        }
        return array;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        Integer[] newArray = new Integer[(toIndex - fromIndex) + 1];
        for (int j = 0; j < newArray.length; j++) {
            for (int i = fromIndex; i <= toIndex; i++) {
                newArray[j++] = list[i];
            }
        }
        return Arrays.asList(newArray);
    }

    @Override
    public boolean removeAll(int[] arr) {
        boolean result = false;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == list[i]) {
                    list[i] = null;
                    break;
                }
            }
        }

        if (arr.length > 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }


    @Override
    public boolean retainAll(int[] arr) {
        boolean result = false;
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] != list[i])
                    list[i] = null;
                break;
            }
        }

        if (list.length > 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}
