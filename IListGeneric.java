package com.company;

import java.util.List;

public interface IListGeneric <E> extends Iterable <E> {
    void clear();
    int size();
    E get(int index);
    boolean add(E number);
    boolean add(int index, E number);
    int remove(E  number);
    int removeByIndex(int index);
    boolean contains(E number);
    String print();
    E [] toArray();
    List<E> subList(int fromIndex, int toIndex);
    boolean removeAll(E[] arr);
    boolean retainAll(E[] arr);
}
