package com.company;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class LListGeneric<E> implements IListGeneric<E> {

    private int size;

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    class Node<E> {
        E e;
        Node<E> next;
        Node<E> prev;

        public Node(E e, Node<E> next, Node<E> prev) {
            this.e = e;
            this.next = next;
            this.prev = prev;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("e=").append(e);
            sb.append(", next=").append(next);
            sb.append(", prev=").append(prev);
            sb.append('}');
            return sb.toString();
        }

        public Node(E e) {
            this.e = e;
            next = null;
            prev = null;
        }
    }


    private Node head;

    public LListGeneric(E[] array) {
        addAll(array);
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {

        if (index < 0)
            throw new RuntimeException("Index is wrong");
        if (index == 0)
            return (E) head.e;
        Node current = null;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Index is wrong");

                current = current.next;
            }
            return (E) current.e;
        }
        throw new RuntimeException("LListGeneric is empty");
    }

    @Override
    public boolean add(E number) {

        try {
            Node new_node = new Node(number);
            Node last = head;
            new_node.next = null;

            if (head == null) {
                new_node.prev = null;
                head = new_node;
                size++;
                return true;
            }

            while (last.next != null)
                last = last.next;
            last.next = new_node;
            new_node.prev = last;
            size++;
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void addAll(E[] arr) {
        for (int i = 0; i < arr.length; i++) {
            add(arr[i]);
        }
    }

    @Override
    public boolean add(int index, E number) {

        try {
            Node node = new Node(number, null, null);
            if (index == 0) {
                addStart(number);
                return true;
            }
            Node ptr = head;
            for (int i = 1; i <= size; i++) {
                if (i == index) {
                    Node temp = ptr.next;
                    ptr.next = node;
                    node.prev = ptr;
                    node.next = temp;
                    temp.prev = node;
                }
                ptr = ptr.next;
            }
            size++;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int remove(E number) {

        try {
            Node helper = new Node(0);
            helper.next = head;
            Node p = helper;

            while (p.next != null) {
                if (p.next.e == number) {
                    Node next = p.next;
                    p.next = next.next;
                } else {
                    p = p.next;
                }
            }
            size--;
            return (int) number;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public int removeByIndex(int index) {

        try {
            Node helper = new Node(0);
            helper.next = head;
            Node p = helper;
            int count = -1;
            while (p.next != null) {
                count++;
                if (count == index) {
                    Node next = p.next;
                    p.next = next.next;
                } else {
                    p = p.next;
                }
                size--;
            }
            return index;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public boolean contains(E number) {

        Node helper = new Node(0);
        helper.next = head;
        Node p = helper;

        while (p.next != null) {
            p = p.next;
            if (p.e == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String print() {

        String result = "";
        try {
            result = Arrays.toString(toArray());
        } catch (NullPointerException e) {
            result = "LListGeneric is empty";
        }
        return result;
    }

    @Override
    public E[] toArray() {

        E[] result = (E[]) new Object[size];
        int i = 0;
        for (Node<E> x = head; x != null; x = x.next)
            result[i++] = x.e;

        return result;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {

        E[] array = toArray();
        int k = 0;
        if (toIndex > size - 1 || toIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is wrong");
        }
        E[] result = (E[]) new Object[(toIndex - fromIndex) + 1];
        for (int i = fromIndex; i <= toIndex; i++) {
            result[k++] = array[i];
        }
        return (List<E>) new LListGeneric(result);
    }

    @Override
    public boolean removeAll(E[] arr) {

        try {
            for (int i = 0; i < arr.length; i++) {
                remove(arr[i]);
            }
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean retainAll(E[] arr) {

        try {
            E[] array = toArray();
            int cont = 0;
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (array[i] == arr[j]) {
                        cont++;
                    }
                }
            }
            E[] result = (E[]) new Object[cont];
            int k = 0;
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < arr.length; j++) {
                    if (array[i] == arr[j]) {
                        result[k++] = array[i];

                    }

                }
            }
            array = result;
            clear();
            addAll(array);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void addStart(E number) {

        Node n = new Node(number);
        n.next = head;
        n.prev = null;

        if (head != null) {
            head.prev = n;
        }
        head = n;

        size++;
    }

    public void addEnd(E number) {
        add(number);
    }
}