package com.company;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class AListIntegerTest {

    @Test
    void clear_ShouldDeleteAllElements() {

        //given
        int[] list = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);

        //when
        aListInteger.clear();

        //then
        assertEquals(0, aListInteger.size());
        assertNotEquals(10, aListInteger.size());
    }


    @Test
    void size_ShouldReturnSize() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);

        //when
        int expected = 3;

        //then
        assertEquals(expected, aListInteger.size());
        assertNotEquals(0, aListInteger.size());
    }

    @Test
    void get_ShouldReturnElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int index = 2;

        //when
        int expected = 3;

        //then
        assertEquals(expected, aListInteger.get(index));
        assertNotEquals(2, aListInteger.get(index));
    }

    @Test
    void add_ShouldAddElement() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aListInteger.add(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void add_ShouldAddElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int number = 2;
        int index = 1;

        //when
        boolean expected = true;
        boolean actual = aListInteger.add(index, number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void remove_ShouldRemoveElement() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int number = 2;

        //when
        int actual = aListInteger.remove(number);

        //then
        assertEquals(number, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void removeByIndex_ShouldRemoveElementByIndex() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int index = 1;

        //when
        int actual = aListInteger.removeByIndex(index);

        //then
        assertEquals(index, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void contains_ShouldCheckElement() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);
        int number = 2;

        //when
        boolean expected = true;
        boolean actual = aListInteger.contains(number);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }

    @Test
    void print_ShouldPrintList() {

        //given
        int list[] = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);

        //when
        String expected = "[1,2,3]";
        String actual = aListInteger.print();

        //then
        assertEquals(expected, actual);
        assertNotEquals("[]", actual);
    }

    @Test
    void toArray_ShouldGetListAsAnArray() {

        //given
        int[] list = {1, 2, 3};
        AListInteger aListInteger = new AListInteger(list);

        //when
        int[] expected = {1, 2, 3};
        int[] actual = aListInteger.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, Arrays.toString(actual));
    }

    @Test
    void subList_ShouldRemoveThePartOfTheList() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        AListInteger aListInteger = new AListInteger(list);
        int fromIndex = 2;
        int toIndex = 3;

        //when
        int[] expected = {3, 4};
        List<Integer> newList = aListInteger.subList(fromIndex, toIndex);
        Object[] actual = newList.toArray();

        //then
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        assertNotEquals(1, actual);
    }

    @Test
    void removeAll_ShouldGetAllElementsWhichAreInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        AListInteger aListInteger = new AListInteger(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aListInteger.removeAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }

    @Test
    void retainAll_ShouldGetAllElementsWhichDoNotRepeatedInBothLists() {

        //given
        int[] list = {1, 2, 3, 4, 5};
        AListInteger aListInteger = new AListInteger(list);
        int[] arr = {1, 2, 3, 0, 0};

        //when
        boolean expected = true;
        boolean actual = aListInteger.retainAll(arr);

        //then
        assertEquals(expected, actual);
        assertNotEquals(1, actual);
    }
}
