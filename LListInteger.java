package com.company;

import java.util.Arrays;
import java.util.List;

public class LListInteger implements IListInteger {

    private int size;

    class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Node{");
            sb.append("val=").append(value);
            sb.append(", next=").append(next);
            sb.append('}');
            return sb.toString();
        }
    }

    private Node head;

    public LListInteger() {
    }

    public LListInteger(int[] array) {
        for (int i = 0; i < array.length; i++) {
            add(array[i]);
        }
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {

        if (index < 0)
            throw new RuntimeException("Index is wrong");
        if (index == 0)
            return head.value;

        Node current = null;
        if (head != null) {
            current = head.next;
            for (int i = 0; i < index - 1; i++) {
                if (current.next == null)
                    throw new RuntimeException("Index is wrong");
                current = current.next;
            }
            return current.value;
        }
        throw new RuntimeException("LListInteger Is Empty");
    }

    @Override
    public boolean add(int number) {

        try {
            if (head == null) {
                head = new Node(number);
                size++;
                return true;

            } else {

                Node tmp = new Node(number);
                Node current = head;
                if (current != null) {
                    while (current.next != null) {
                        current = current.next;
                    }
                    current.setNext(tmp);
                }
                size++;
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean add(int index, int number) {

        try {
            Node temp = new Node(number);
            Node current = head;

            if (current != null) {

                for (int i = 0; i < index - 1 && current.next != null; i++) {
                    current = current.next;
                }
            }

            temp.setNext(current.next);
            current.setNext(temp);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    public int remove(int number) {

        try {
            int index = 0;
            Node current = head;

            while (current.value != number) {
                current = current.next;
                index++;

            }
            removeByIndex(index);
            return number;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public int removeByIndex(int index) {

        try {
            Node current = head;
            if (index < 0 || index > size())
                throw new RuntimeException("Index is wrong");
            if (index == 0) {
                head = head.next;
            }
            if (head != null) {

                for (int i = 0; i < index - 1; i++) {
                    if (current.next == null)
                        throw new RuntimeException("Index is wrong");
                    current = current.next;
                }
                current.setNext(current.next.next);
                size--;
            }
            return index;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public boolean contains(int number) {

        Node current = head;
        if (current.value == number) {
            return true;
        }
        while (current.next != null) {

            current = current.next;
            if (current.value == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String print() {

        StringBuilder sb = new StringBuilder();
        sb.append("[");

        if (head == null) {
            sb.append(" ");
        } else {
            Node temp = head;

            while (temp != null) {
                sb.append(temp.value);
                sb.append(",");
                temp = temp.next;
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int[] toArray() {

        int[] array = new int[size];
        int k = 1;
        Node current = head;
        array[0] = current.value;
        while (current.next != null) {

            current = current.next;
            array[k++] = current.value;
        }
        return array;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {

        int[] array = toArray();
        if (toIndex > size - 1 || toIndex < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is wrong");
        }
        
        Integer[] result = new Integer[(toIndex - fromIndex) + 1];
        int j = 0;
        for (int i = fromIndex; i <= toIndex; i++) {
            result[j++] = array[i];
        }
        return Arrays.asList(result);
    }

    @Override
    public boolean removeAll(int[] arr) {

        try {
            for (int value : arr) {
                remove(value);
            }
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean retainAll(int[] arr) {

        try {
            for (int value : arr) {
                if (!contains(value)) {
                    add(value);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean addStart(int number) {

        try {
            Node newNode = new Node(number);

            if (head == null) {
                head = newNode;

            } else {

                Node temp = head;
                head = newNode;
                head.next = temp;
            }
            size++;
            return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public boolean addEnd(int number) {
        return add(number);
    }
}

